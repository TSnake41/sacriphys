return {
  filter = { "thruster" },
  process = {
    update = function (self, dt)
      for _, entity in ipairs(self.entities) do
        local t = entity.thruster
        local power = t.power

        local entity_v = entity.velocity

        -- Make thrusters unstable
        power = power + dt * 24

        local v = {
          math.sin(entity.angle) * dt * power,
          -math.cos(entity.angle) * dt * power
        }

        entity_v.x = -v[1] + entity_v.x
        entity_v.y = v[2] + entity_v.y

        t.power = power - dt * (power / 32)

        t.delta = t.delta + dt
        while t.delta > (10 / power) do
          -- Emit particle
          entity.particles[#entity.particles+1] = {
            entity.x - entity.w/2 * math.sin(entity.angle),
            entity.y - entity.h/2 * -math.cos(entity.angle),
            4,
            nil,
            power/5 * -v[1],
            power/5 * -v[2]
          }

          t.delta = t.delta - (10 / power)
        end
      end
    end
  }
}

local maid64 = require "lib.maid64"

return {
  filter = {"x", "y"},
  process = {
    draw = function (self)
      maid64.start()
      camera:attach()

      for _, entity in ipairs(self.entities) do
        love.graphics.printf(
          string.format("  %s\r\n(%d;%d %d)", entity.name or "unknown", entity.x, entity.y, entity.angle * 180 / math.pi),
          entity.x, entity.y, 250, "left", 0, 1, 1, entity.w / 2, entity.h / 2)
      end

      camera:detach()
      maid64.finish()
    end
  }
}

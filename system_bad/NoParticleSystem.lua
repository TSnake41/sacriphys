return {
  filter = { "particle" },
  process = {
    update = function (self, dt)
      for _, entity in ipairs(self.entities) do
        if #entity.particles > 0 then
          entity.particles = {}
        end
      end
    end
  }
}

local util = require "util"
local entropy = 1
local delta_entropy = 0

return {
	filter = { "x", "y", "collision" },
	process = {
		update = function(self, dt)
      delta_entropy = delta_entropy + dt

      if delta_entropy >= 5 then
        delta_entropy = delta_entropy - 5

        entropy = math.random(-.1, .1)
      end

			for i = 1, #self.entities - 1 do
				for j = i + 1, #self.entities do
					local a = self.entities[i]
					local b = self.entities[j]
					if util.dist(a.x, a.y, b.x, b.y) <= (a.collision.radius - entropy + b.collision.radius) then
						self:trigger("collide", a, b)
						self:trigger("collide", b, a)
					end
				end
			end
		end
	},
  on = {
    collide = function (self, a, b)
      local function f(a, b)
        if a.asteroid then
          util.place_asteroid(a)

          local p = #a.particles
          local p_count = math.ceil(math.random() * 12)

          for i=1,6 do
            a.particles[p + i] = {
              a.x, a.y,
              6,
              nil,
              math.random() * math.cos(i * math.pi / 3) * 5,
              math.random() * math.sin(i * math.pi / 3) * 5
            }
          end
        end

        if a.player then
          camera:shake(10, 1)
          a.life = a.life - 0.5
          return
        end

        camera:shake(1, 1)
      end

      f(a, b)
      f(b, a)

      love.audio.play(destroy_sound)
      self:trigger "destroy"
    end
  }
}

local entropy = 1
local delta_entropy = 0

return {
  filter = { "x", "y", "velocity" },
  process = {
    update = function (self, dt)
      delta_entropy = delta_entropy + dt

      if delta_entropy >= 5 then
        delta_entropy = delta_entropy - 5

        entropy = math.random(-.1, .1)
      end

      for _, entity in ipairs(self.entities) do
        local v = entity.velocity
        entity.x = entity.x + v.x * dt + v.x * dt * entropy * 3
        entity.y = entity.y + v.y * dt + v.y * dt * entropy * 2

        -- Put entropy in thrusters
        entity.angle = (entity.angle + v.angular * dt + v.angular * dt * entropy) % (math.pi * 2)

        v.x = v.x - v.x * dt
        v.y = v.y - v.y * dt
        v.angular = v.angular - v.angular * dt
      end
    end
  }
}

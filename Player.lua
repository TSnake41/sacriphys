return function (sprite)
  local self = {}

  self.player = true
  self.name = "player"

  self.life = 10

  self.x = 50
  self.y = 50
  self.angle = 0

  self.w = 51
  self.h = 48

  self.collision = {
    radius = 24
  }

  self.particles = {}

  self.order = 2

  self.sprite = sprite
  self.quad = love.graphics.newQuad(0, 0, 51, 48, sprite:getDimensions())

  self.velocity = { x = 0, y = 0, angular = 0 }

  self.thruster = {
    power = 0,
    delta = 0
  }

  return self
end

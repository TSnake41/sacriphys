local util = require "util"

return function (old)
  self = old or {}

  self.order = 1
  self.name = "asteroid"

  self.asteroid = {
    velocity = {}
  }

  self.velocity = { x = 0, y = 0, angular = 0 }
  self.particles = {}

  -- Place astroid
  util.place_asteroid(self)

  -- shape asteroid
  local size = math.random(0.75, 1) * 16

  self.w, self.h = size, size

  self.verticles = {}

  self.collision = {
    radius = size * 1.25
  }

  for i=1,8 do
    local height = size + (math.random() - 1 * .1) * size

    local alpha = (math.pi * 2 / 8) * i

    self.verticles[2 * i - 1] = math.cos(alpha) * height
    self.verticles[2 * i] = math.sin(alpha) * height
  end

  return self
end

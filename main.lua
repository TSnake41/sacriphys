local nata = require "lib.nata"
local baton = require "lib.baton"
local maid64 = require "lib.maid64"

sacriphys = require "sacriphys"

local pool = nata.new(sacriphys.nata_systems)

sacriphysing = false
game_over = false

local player_input = baton.new {
  controls = {
    left = { "key:left", "axis:leftx-", "button:dpleft" },
    right = { "key:right", "axis:leftx+", "button:dpright" },
    up = { "key:up", "axis:lefty-", "button:dpup" },
    down = { "key:down", "axis:lefty+", "button:dpdown" },
    quit = { "key:escape" }
  },
  joystick = love.joystick.getJoysticks()[1],
  deadzone = .33,
}

function love.load()
  maid64.setup(screen_width, screen_width)
  love.graphics.setDefaultFilter("nearest", "nearest")

  camera = (require "lib.Camera")(screen_width/2, screen_width/2, screen_width, screen_width)

  local Player = require "Player"
  local Asteroid = require "Asteroid"

  font = love.graphics.newFont("assets/Boxy-Bold.ttf", 14)
  font:setFilter("nearest")

  ship_sprite = love.graphics.newImage "assets/ship.png"

  camera:setFollowStyle('LOCKON')
  camera:setFollowLerp(0.5)
  camera:setFollowLead(10)

  pool:queue(Player(ship_sprite))
  pool:queue { time = 0 }

  -- Generate 24 asteroids
  for i=1,24 do
    pool:queue(Asteroid())
  end

  pool:flush()
end

function love.update(dt)
  player_input:update()

  if player_input:pressed "quit" then
    love.event.quit()
  end

  if sacriphysing then
    sacriphys.update(dt, player_input)
  elseif not game_over then
    camera:update(dt)
    pool:process("update", dt, player_input)
  end
end

function love.draw()
  if sacriphysing then
    sacriphys.draw()
  else
    pool:process("draw")
  end

  if game_over then
    maid64.start()
    love.graphics.setFont(font)
    love.graphics.printf("GAME OVER", 0, 128, 256, "center", 0, 2, 2)
    love.graphics.printf("Press escape to quit", 0, 196*2, 256*2, "center", 0, 1, 1)
    maid64.finish()
  end
end

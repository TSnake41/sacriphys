local maid64 = require "lib.maid64"

return {
  filter = { "player" },

  process = {
    update = function (self, dt, input)
      for _, entity in ipairs(self.entities) do
        if entity.life == 0 then
          love.audio.play(love.audio.newSource("assets/Death.ogg", "static"))
          game_over = true
        end

        camera:follow(entity.x, entity.y)

        local v = entity.velocity
        local t = entity.thruster

        if input:down "left" then
          v.angular = v.angular - dt * 3
        end

        if input:down "right" then
          v.angular = v.angular + dt * 3
        end

        if input:down "up" then
          t.power = t.power + dt * 32
        end

        if input:down "down" then
          t.power = t.power - t.power * (dt / 3)
        end
      end
    end,
    draw = function (self)
      maid64.start()
      for _,entity in ipairs(self.entities) do
        love.graphics.setColor(1,1,1,1)
        love.graphics.setFont(font)
        love.graphics.printf({ { 0, 1, 0, 1 }, string.format("%d HP", entity.life) }, 0, 256 + 32, screen_width, "center", 0, 2, 2, 128, 128)
      end
      maid64.finish()
    end
  }
}

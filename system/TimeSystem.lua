local maid64 = require "lib.maid64"

return {
  filter = { "time" },

  process = {
    update = function (self, dt)
      for _,entity in ipairs(self.entities) do
        entity.time = entity.time + dt
        entity.delta = (entity.delta or 0) + dt

        if entity.delta > 15 then
          entity.delta = entity.delta - 15
          sacriphysing = true
        end
      end
    end,
    draw = function (self)
      maid64.start()
      for _,entity in ipairs(self.entities) do
        love.graphics.setColor(1,1,1,1)
        love.graphics.setFont(font)
        love.graphics.printf({ { 1, 1, 0, 1 }, string.format("%.1f", entity.time)}, 0, 256, screen_width, "center", 0, 2, 2, 128, 128)
      end
      maid64.finish()

      love.graphics.reset( )
    end
  }
}

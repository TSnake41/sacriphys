local Asteroid = require "Asteroid"
local util = require "util"

return {
  filter = { "asteroid" },
  process = {
    update = function (self)
      for _, entity in ipairs(self.entities) do
        if util.dist(entity.x, entity.y, camera.x, camera.y) > (2 * screen_width) then
          -- replace asteroid
          util.place_asteroid(entity)
        end

        -- Make velocity constant
        entity.velocity.x = entity.asteroid.velocity.x
        entity.velocity.y = entity.asteroid.velocity.y
        entity.velocity.angular = entity.asteroid.velocity.angular
      end
    end
  }
}

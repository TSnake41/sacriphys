return {
  filter = { "x", "y", "velocity" },
  process = {
    update = function (self, dt)
      for _, entity in ipairs(self.entities) do
        local v = entity.velocity
        entity.x = entity.x + v.x * dt
        entity.y = entity.y + v.y * dt

        entity.angle = (entity.angle + v.angular * dt) % (math.pi * 2)

        v.x = v.x - v.x * dt
        v.y = v.y - v.y * dt
        v.angular = v.angular - v.angular * dt
      end
    end
  }
}

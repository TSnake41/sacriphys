return {
  filters = "true",
  init = function (self)
    destroy_sound = love.audio.newSource("assets/Explosion.ogg", "static")
  end,
  process = {
    update = function (self)
    end
  },
  on = {
    destroy = function (self)
      print(self.destroy_sound)
      love.audio.play(self.destroy_sound)
    end
  }
}

return {
  filter = { "particles" },
  process = {
    update = function (self, dt)
      for _, entity in ipairs(self.entities) do
        local to_remove = {}
        for i,v in ipairs(entity.particles) do
          v[3] = v[3] - dt

          if v[5] and v[6] then
            v[1] = v[1] + v[5] * dt
            v[2] = v[2] + v[6] * dt
          end

          if v[3] <= 0 then
            to_remove[#to_remove + 1] = i
          end
        end

        -- Reverse to remove
        for i=#to_remove,1,-1 do
          table.remove(entity.particles, i)
        end
      end
    end
  }
}

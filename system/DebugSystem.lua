local maid64 = require "lib.maid64"
return {
  filter = {},

  process = {
    draw = function (self)
      camera:attach()
      maid64.start()

      love.graphics.setLineWidth(2)

      for _, entity in ipairs(self.entities) do
        if entity.velocity then
          local v = entity.velocity

          local dx, dy = entity.x + v.x, entity.y + v.y
          love.graphics.push()
          love.graphics.setColor(0, 1, .2, 1)
          love.graphics.line(entity.x, entity.y, dx, dy)

          love.graphics.setColor(0, .2, 1, 1)

          love.graphics.arc("line", "open", entity.x, entity.y, 32,
            entity.angle - math.pi / 2,
            entity.angle + v.angular - math.pi / 2,
            16)
          love.graphics.pop()
        end

        if entity.collision then
          love.graphics.push()
          love.graphics.setColor(1, 0, 0, 1)
          love.graphics.circle("line", entity.x, entity.y, entity.collision.radius)
          love.graphics.pop()
        end
      end

      love.graphics.setColor(1, 1, 1, 1)

      camera:detach()
      maid64.finish()
    end
  }
}

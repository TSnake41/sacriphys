local maid64 = require "lib.maid64"

return {
  filter = { "x", "y" },
  sort = function (a, b)
    return a.order < b.order
  end,

  process = {
    draw = function (self)
      maid64.start()

      camera:attach()
      for _, entity in ipairs(self.entities) do

        -- Draw particles
        if entity.particles then
          love.graphics.push()

          for i,v in ipairs(entity.particles) do
            love.graphics.setPointSize(v[4] or 3)
            love.graphics.setColor(1, 1, 1, v[3] / 4)
            love.graphics.points(v[1], v[2])
          end
          love.graphics.pop()
          love.graphics.setColor(1, 1, 1, 1)
        end

        -- Draw asteroids
        if entity.verticles then
          love.graphics.push()

          love.graphics.setLineWidth(2)
          love.graphics.translate(entity.x, entity.y)
          love.graphics.rotate(entity.angle)
          love.graphics.polygon("line", entity.verticles)

          love.graphics.pop()
        end

        -- Draw ships.
        if entity.sprite then
          love.graphics.draw(
            entity.sprite,
            entity.quad,
            entity.x,
            entity.y,
            entity.angle,
            1, 1, entity.w / 2, entity.h / 2)
        end
      end
      camera:detach()

      maid64.finish()
    end
  }
}

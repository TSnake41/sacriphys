local sacriphys = {}
local maid64 = require "lib.maid64"

local nata_systems = {}

local systems = {
  Animation = {
    original = require "system.ParticleSystem",
    replace = require "system_bad.NoParticleSystem",
    message = "No more particles.",
    sacrifiable = true
  },
  Asteroid = {
    original = require "system.AsteroidSystem",
    replace = require "system_bad.EnemyShipSystem",
    message = "No more asteroid, but something else to replace them !",
    sacrifiable = true
  },
  Collision = {
    original = require "system.CollisionSystem",
    replace = require "system_bad.MessyCollisionSystem",
    message = "Put entropy in collisions.",
    sacrifiable = true
  },
  Renderer = {
    original = require "system.RendererSystem",
    replace = require "system_bad.TextRendererSystem",
    message = "No more fancy rendering.",
    sacrifiable = true
  },
  Velocity = {
    original = require "system.VelocitySystem",
    replace = require "system_bad.MessyVelocitySystem",
    message = "Unpredicatble movements.",
    sacrifiable = true
  },
  Sound = {
    original = require "system.SoundSystem",
    message = "Quieter game.",
    sacrifiable = true
  },
  Thruster = {
    original = require "system.ThrusterSystem",
    replace = require "system_bad.MessyThrusterSystem",
    message = "Very glitchy thrusters",
    sacrifiable = true
  },
  Debug = {
    original = require "system.DebugSystem",
    message = "No more debug stuff.",
    sacrifiable = true
  },
  _player = {
    original = require "system.PlayerSystem"
  },
  _time = {
    original = require "system.TimeSystem"
  }
}

local system_pool = {}

local i = 1
for _,v in pairs(systems) do
  local ns = {}

  -- Duplicate system (no need of deep copy)
  for k,v in pairs(v.original) do
    ns[k] = v
  end

  if v.sacrifiable then
    system_pool[i] = _
  end

  nata_systems[i] = ns
  v.n = i

  i = i + 1
end

local choices = {}
local choices_n = {}

function sacriphys.draw()
  if not sacriphysing or #choices == 0 then
    return
  end

  maid64.start()

  -- Draw 2 rectangles
  love.graphics.setLineWidth(2)

  --love.graphics.rectangle(mode, x, y, width, height)

  love.graphics.rectangle("line", 0, 5, 256 - 10, 256 - 10)
  love.graphics.rectangle("line", 256, 5, 256 - 10, 256 - 10)

  love.graphics.setFont(font)
  love.graphics.printf({ { 1, .5, 0, 1 }, "Sacriphys something" }, 0, 2, 256, "center", 0, 2, 2)

  love.graphics.printf(choices[1], 8, 64, 128, "center", 0, 2, 2)
  love.graphics.printf(choices[2], 256, 64, 128, "center", 0, 2, 2)

  love.graphics.printf(systems[choices[1]].message, -64, 128, 256, "center", 0, 1.5, 1.5)
  love.graphics.printf(systems[choices[2]].message, 192, 128, 256, "center", 0, 1.5, 1.5)

  maid64.finish()
end

function sacriphys.update(dt, player_input)
  if #choices == 0 then
    if #system_pool == 0 then
      sacriphysing = false
    end

    choices_n[1] = math.floor(math.random(1, #system_pool))
    choices_n[2] = math.floor(math.random(1, #system_pool))

    choices[1] = system_pool[choices_n[1]]
    choices[2] = system_pool[choices_n[2]]
  end

  if player_input:pressed "left" then
    sacriphys.replace_system(choices[1])
    table.remove(system_pool, choices_n[1])
    choices[1] = nil
    choices[2] = nil

    sacriphysing = false
  end

  if player_input:pressed "right" then
    sacriphys.replace_system(choices[2])
    table.remove(system_pool, choices_n[2])

    choices[1] = nil
    choices[2] = nil

    sacriphysing = false
  end
end

-- Replace functions and stuff.
function sacriphys.replace_system(name)
  local s = systems[name]
  if not s then
    return
  end

  local system = nata_systems[s.n]
  local replacement = s.replace

  if not replacement then
    system.filter = function () return false end
    system.process = {}
    system.on = {}
  else
    for k,v in pairs(system) do
      system[k] = replacement[k]
    end
  end
end

sacriphys.systems = systems
sacriphys.nata_systems = nata_systems

return sacriphys

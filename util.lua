local util = {}

function util.dist(x_from, y_from, x_to, y_to)
  return math.sqrt((x_from - x_to)^2 + (y_from - y_to)^2)
end

-- util.place_asteroid(asteroid, camera)
function util.place_asteroid(asteroid)
  local angle = math.random() * math.pi * 2

  local vx, vy = math.cos(angle), math.sin(angle)

  asteroid.x = (camera.x - vx * screen_width)
  asteroid.y = (camera.y - vy * screen_width)

  asteroid.angle = 0

  asteroid.asteroid.velocity = {
    x = vx * math.random(4, 16),
    y = vy * math.random(4, 16),
    angular = angle / 4
  }
end

return util
